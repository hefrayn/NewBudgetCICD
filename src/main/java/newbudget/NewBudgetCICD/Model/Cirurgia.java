package newbudget.NewBudgetCICD.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Cirurgia {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotEmpty
	@Size(max = 100)
	private String nome;
	
	@NotEmpty
	@Size(max = 200)
	private String descricao;
	
	@NotEmpty
	private double valortabela;
	
	@NotEmpty
	private double valorminimo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValortabela() {
		return valortabela;
	}

	public void setValortabela(double valortabela) {
		this.valortabela = valortabela;
	}

	public double getValorminimo() {
		return valorminimo;
	}

	public void setValorminimo(double valorminimo) {
		this.valorminimo = valorminimo;
	}
	
	
	
	
}
