package newbudget.NewBudgetCICD.Model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

@Entity
public class CirurgiaOrcamento {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotEmpty
	private double valororcado;
	
	@NotEmpty
	private double desconto;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_orcamento")
	private Orcamento orcamento;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_cirurgia")
	private Cirurgia cirurgia;

	public double getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getValororcado() {
		return valororcado;
	}

	public void setValororcado(double valororcado) {
		this.valororcado = valororcado;
	}

	public double getDesconto() {
		return desconto;
	}

	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}

	public Orcamento getOrcamento() {
		return orcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public Cirurgia getCirurgia() {
		return cirurgia;
	}

	public void setCirurgia(Cirurgia cirurgia) {
		this.cirurgia = cirurgia;
	}
	
	
	
	
}
