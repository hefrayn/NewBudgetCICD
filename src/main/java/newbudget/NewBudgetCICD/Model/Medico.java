package newbudget.NewBudgetCICD.Model;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Medico extends Pessoa{
	
	
	@NotEmpty
	@Size(max = 8)
	private String crm;

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	
}
