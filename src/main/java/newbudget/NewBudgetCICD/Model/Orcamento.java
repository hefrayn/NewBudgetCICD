package newbudget.NewBudgetCICD.Model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Orcamento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private Date datahora;
	private String observacao;
	private double valortotal;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_funcionario")
	private Funcionario funcionario;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_medico")
	private Medico medico;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_paciente")
	private Paciente paciente;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Date getDatahora() {
		return datahora;
	}


	public void setDatahora(Date datahora) {
		this.datahora = datahora;
	}


	public String getObservacao() {
		return observacao;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}


	public double getValortotal() {
		return valortotal;
	}


	public void setValortotal(double valortotal) {
		this.valortotal = valortotal;
	}


	public Funcionario getFuncionario() {
		return funcionario;
	}


	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}


	public Medico getMedico() {
		return medico;
	}


	public void setMedico(Medico medico) {
		this.medico = medico;
	}


	public Paciente getPaciente() {
		return paciente;
	}


	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}	
}
