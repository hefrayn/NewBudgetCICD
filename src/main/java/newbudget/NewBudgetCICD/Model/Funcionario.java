package newbudget.NewBudgetCICD.Model;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Funcionario extends Pessoa {
	
	@NotEmpty
	@Size(max = 15)
	private String login;
	
	@NotEmpty
	@Size(max = 15)
	private String senha;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	
}
