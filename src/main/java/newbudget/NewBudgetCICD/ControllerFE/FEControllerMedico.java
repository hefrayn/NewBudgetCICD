package newbudget.NewBudgetCICD.ControllerFE;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import newbudget.NewBudgetCICD.Model.Medico;
import newbudget.NewBudgetCICD.Services.MedicoService;

@Controller
public class FEControllerMedico {
	
	@Autowired
	private MedicoService service;

	@GetMapping("/listar_medicos")
	public ModelAndView findAll(String mensagem) {
		ModelAndView mv = new ModelAndView("medico/lstmed");
		mv.addObject("mensagem", mensagem);
		mv.addObject("medicos", service.findAll());
		return mv;
	}
	
	@GetMapping("/cadastrar_medico")
	public ModelAndView add(Medico medico, String mensagem) {
		
		ModelAndView mv = new ModelAndView("medico/cadmed");
		mv.addObject("mensagem", mensagem);
		mv.addObject("medico", medico);
		
		return mv;
	}
	
	@PostMapping("/save_medico")
	public ModelAndView save(@Valid Medico medico, BindingResult result) {
		if(result.hasErrors()) {
			return add(medico, "Verifique os dados do Medico");
		}
		
		service.save(medico);
		return findAll("Medico Salvo Com Sucesso");
	}
	
	@GetMapping("/deletar_medico")
	public ModelAndView delete(long id) {
		
		service.delete(id);
		
		return findAll("Medico deletado Com Sucesso.");
	}
	
	@GetMapping("/editar_medico")
	public ModelAndView edit(long id) {
		
		return add(service.findOne(id), "Edite os dados do Medico");
	}
	
	@PostMapping("/buscar_medico_by_id")
	public ModelAndView buscarMedicoPorCodigo(long id) {
		ModelAndView mv = new ModelAndView("medico/lstmed");
		mv.addObject("medicos", service.findOne(id));
		return mv;
	}
	
	@PostMapping("/buscar_medico_by_nome")
	public ModelAndView buscarMedicoPorNome(String nome) {
		ModelAndView mv = new ModelAndView("medico/lstmed");
		mv.addObject("medicos", service.findName(nome));
		return mv;
	}
	
	
	@PostMapping("/buscar_medico_by_datanascimento")
	public ModelAndView buscarMedicoPorDatanascimento(String datanascimento) {
		ModelAndView mv = new ModelAndView("medico/lstmed");
		mv.addObject("medicos", service.findDatanascimento(datanascimento));
		return mv;
	}
	
	@PostMapping("/buscar_medico_by_cpf")
	public ModelAndView buscarMedicoPorCpf(String cpf) {
		ModelAndView mv = new ModelAndView("medico/lstmed");
		mv.addObject("medicos", service.findCpf(cpf));
		return mv;
	}
	
	
	@PostMapping("/buscar_medico_by_crm")
	public ModelAndView buscarMedicoPorCrm(String crm) {
		ModelAndView mv = new ModelAndView("medico/lstmed");
		mv.addObject("medicos", service.findCrm(crm));
		return mv;
	}
	
	@PostMapping("/buscar_medico_by_celular")
	public ModelAndView buscarMedicoPorCelular(String celular) {
		ModelAndView mv = new ModelAndView("medico/lstmed");
		mv.addObject("medicos", service.findCelular(celular));
		return mv;
	}
}
