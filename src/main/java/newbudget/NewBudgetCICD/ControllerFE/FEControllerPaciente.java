package newbudget.NewBudgetCICD.ControllerFE;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import newbudget.NewBudgetCICD.Model.Paciente;
import newbudget.NewBudgetCICD.Services.PacienteService;

@Controller
public class FEControllerPaciente {
	
	@Autowired
	private PacienteService service;

	
	@GetMapping("/listar_pacientes")
	public ModelAndView findAll(String mensagem) {
		ModelAndView mv = new ModelAndView("paciente/lstpac");
		mv.addObject("mensagem", mensagem);
		mv.addObject("pacientes", service.findAll());
		return mv;
	}
	
	@GetMapping("/cadastrar_paciente")
	public ModelAndView add(Paciente paciente, String mensagem) {
		
		ModelAndView mv = new ModelAndView("paciente/cadpac");
		mv.addObject("mensagem", mensagem);
		mv.addObject("paciente", paciente);
		
		return mv;
	}
	
	@PostMapping("/save_paciente")
	public ModelAndView save(@Valid Paciente paciente, BindingResult result) {
		if(result.hasErrors()) {
			return add(paciente, "Verifique os dados do Paciente");
		}
		
		service.save(paciente);
		return findAll("Paciente Salvo Com Sucesso");
	}
	
	@GetMapping("/deletar_paciente")
	public ModelAndView delete(long id) {
		
		service.delete(id);
		
		return findAll("Paciente deletado Com Sucesso.");
	}
	
	@GetMapping("/editar_paciente")
	public ModelAndView edit(long id) {
		
		return add(service.findOne(id), "Edite os dados do Paciente");
	}
	
	@PostMapping("/buscar_paciente_by_id")
	public ModelAndView buscarPacientePorCodigo(long id) {
		ModelAndView mv = new ModelAndView("paciente/lstpac");
		mv.addObject("pacientes", service.findOne(id));
		return mv;
	}
	
	@PostMapping("/buscar_paciente_by_nome")
	public ModelAndView buscarPacientePorNome(String nome) {
		ModelAndView mv = new ModelAndView("paciente/lstpac");
		mv.addObject("pacientes", service.findName(nome));
		return mv;
	}
	
	
	@PostMapping("/buscar_paciente_by_datanascimento")
	public ModelAndView buscarPacientePorDatanascimento(String datanascimento) {
		ModelAndView mv = new ModelAndView("paciente/lstpac");
		mv.addObject("pacientes", service.findDatanascimento(datanascimento));
		return mv;
	}
	
	@PostMapping("/buscar_paciente_by_cpf")
	public ModelAndView buscarPacientePorCpf(String cpf) {
		ModelAndView mv = new ModelAndView("paciente/lstpac");
		mv.addObject("pacientes", service.findCpf(cpf));
		return mv;
	}
	
	@PostMapping("/buscar_paciente_by_celular")
	public ModelAndView buscarPacientePorCelular(String celular) {
		ModelAndView mv = new ModelAndView("paciente/lstpac");
		mv.addObject("pacientes", service.findCelular(celular));
		return mv;
	}
}
