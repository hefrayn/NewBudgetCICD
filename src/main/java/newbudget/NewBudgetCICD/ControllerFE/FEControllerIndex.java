package newbudget.NewBudgetCICD.ControllerFE;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FEControllerIndex {

	@RequestMapping("/index")
	public String index() {
		return "index";
	}
	
}
