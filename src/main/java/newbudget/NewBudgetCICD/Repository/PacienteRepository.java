package newbudget.NewBudgetCICD.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import newbudget.NewBudgetCICD.Model.Paciente;

@Repository
public interface PacienteRepository extends JpaRepository<Paciente, Long>{
	
	Paciente findById(long id);
	Paciente deleteById(long id);
	List<Paciente> findByNome(String nome);
	List<Paciente> findByDatanascimento(String datanascimento);
	Paciente findByCpf(String cpf);
	List<Paciente> findByCelular(String celular);
	
	

}
