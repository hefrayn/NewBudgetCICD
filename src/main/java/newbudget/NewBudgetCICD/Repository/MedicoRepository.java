package newbudget.NewBudgetCICD.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import newbudget.NewBudgetCICD.Model.Medico;

public interface MedicoRepository extends JpaRepository<Medico, Long>{

	Medico findById(long id);
	Medico deleteById(long id);
	List<Medico> findByNome(String nome);
	List<Medico> findByDatanascimento(String datanascimento);
	Medico findByCpf(String cpf);
	Medico findByCrm(String crm);
	List<Medico> findByCelular(String celular);
}
