package newbudget.NewBudgetCICD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewBudgetCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewBudgetCicdApplication.class, args);
	}
}
