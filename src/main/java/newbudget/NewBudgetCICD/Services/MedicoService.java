package newbudget.NewBudgetCICD.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import newbudget.NewBudgetCICD.Model.Medico;
import newbudget.NewBudgetCICD.Repository.MedicoRepository;

@Service
public class MedicoService {
	
	@Autowired
	private MedicoRepository repository;

	public List<Medico> findAll(){
		return repository.findAll();
	}
	
	public Medico save(Medico medico) {
		return repository.saveAndFlush(medico);
		
	}

	public Medico delete(long id) {
		return repository.deleteById(id);
	}

	public Medico findOne(long id) {
		return repository.findById(id);
	}
	
	
	public List<Medico> findName(String nome) {
		return repository.findByNome(nome);
	}

	public List<Medico> findDatanascimento(String datanascimento) {
		return repository.findByDatanascimento(datanascimento);
	}

	public Medico findCpf(String cpf) {
		return repository.findByCpf(cpf);
	}
	
	public Medico findCrm(String crm) {
		return repository.findByCrm(crm);
	}
	public List<Medico> findCelular(String celular) {
		return repository.findByCelular(celular);
	}
}
