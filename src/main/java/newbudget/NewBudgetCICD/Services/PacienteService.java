package newbudget.NewBudgetCICD.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import newbudget.NewBudgetCICD.Model.Paciente;
import newbudget.NewBudgetCICD.Repository.PacienteRepository;

@Service
public class PacienteService {

	@Autowired
	private PacienteRepository repository;
	
	public List<Paciente> findAll(){
		return repository.findAll();
	}

	public Paciente save(Paciente paciente) {
		return repository.saveAndFlush(paciente);
		
	}

	public Paciente delete(long id) {
		return repository.deleteById(id);
	}

	public Paciente findOne(long id) {
		return repository.findById(id);
	}
	
	
	public List<Paciente> findName(String nome) {
		return repository.findByNome(nome);
	}

	public List<Paciente> findDatanascimento(String datanascimento) {
		return repository.findByDatanascimento(datanascimento);
	}

	public Paciente findCpf(String cpf) {
		return repository.findByCpf(cpf);
	}
	
	public List<Paciente> findCelular(String celular) {
		return repository.findByCelular(celular);
	}
}
