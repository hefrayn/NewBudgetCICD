package newbudget.NewBudgetCICD.Services;

import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import newbudget.NewBudgetCICD.Model.Medico;
import newbudget.NewBudgetCICD.Repository.MedicoRepository;

@RunWith(MockitoJUnitRunner.class)
public class MedicoServiceTest {

	@InjectMocks
	private MedicoService service;

	@Mock
	private MedicoRepository repository;
	
	@Mock 
	private Medico medico;
	
	@Test
	public void BuscarTodosMedicos() {
		List<Medico> list = service.findAll();		
		Assert.assertTrue(list instanceof List);
	}
	
	@Test
	public void salvarMedico () {

		when(service.save(medico)).thenReturn(medico);
		Assert.assertTrue(service.save(medico) instanceof Medico);
	}
	
	@Test
	public void deletarMedico() {
		
		when(service.delete(5)).thenReturn(medico);
		Assert.assertTrue(service.delete(5) instanceof Medico);
		
	}
	
	@Test
	public void buscarUmMedico() {
		when(service.findOne(5)).thenReturn(medico);
		when(service.findOne(5).getNome()).thenReturn("Nome Medico");
		Assert.assertTrue(service.findOne(5).getNome() == "Nome Medico");
	}
	
	@Test
	public void buscarMedicoPorNome() {
		List<Medico> list = service.findName("Hefrayn Antero");		
		Assert.assertTrue(list instanceof List);
	}
	
	@Test
	public void buscarMedicoPorDatanascimento() {
		List<Medico> list = service.findDatanascimento("19/04/1993");		
		Assert.assertTrue(list instanceof List);
	}
	
	@Test
	public void buscarMedicoPorCpf () {

		when(service.findCpf("04849633170")).thenReturn(medico);
		Assert.assertTrue(service.findCpf("04849633170") instanceof Medico);
	}
	
	@Test
	public void buscarMedicoPorCrm () {

		when(service.findCrm("2918")).thenReturn(medico);
		Assert.assertTrue(service.findCrm("2918") instanceof Medico);
	}
	
	@Test
	public void buscarMedicoPorCelular() {
		List<Medico> list = service.findCelular("62994864272");		
		Assert.assertTrue(list instanceof List);
	}
	
	
}
