package newbudget.NewBudgetCICD.Services;

import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import newbudget.NewBudgetCICD.Model.Paciente;
import newbudget.NewBudgetCICD.Repository.PacienteRepository;

@RunWith(MockitoJUnitRunner.class)
public class PacienteServiceTest {
	
	@InjectMocks
	private PacienteService service;

	@Mock
	private PacienteRepository repository;
	
	@Mock 
	private Paciente paciente;
	
	@Test
	public void BuscarTodosPacientes() {
		List<Paciente> list = service.findAll();		
		Assert.assertTrue(list instanceof List);
	}
	
	@Test
	public void salvarPaciente () {

		when(service.save(paciente)).thenReturn(paciente);
		Assert.assertTrue(service.save(paciente) instanceof Paciente);
	}
	
	@Test
	public void deletarPaciente() {
		
		when(service.delete(5)).thenReturn(paciente);
		Assert.assertTrue(service.delete(5) instanceof Paciente);
		
	}
	
	@Test
	public void buscarUmPaciente() {
		when(service.findOne(5)).thenReturn(paciente);
		when(service.findOne(5).getNome()).thenReturn("Nome Paciente");
		Assert.assertTrue(service.findOne(5).getNome() == "Nome Paciente");
	}
	
	@Test
	public void buscarPacientePorNome() {
		List<Paciente> list = service.findName("Hefrayn Antero");		
		Assert.assertTrue(list instanceof List);
	}
	
	@Test
	public void buscarPacientePorDatanascimento() {
		List<Paciente> list = service.findDatanascimento("19/04/1993");		
		Assert.assertTrue(list instanceof List);
	}
	
	@Test
	public void buscarPacientePorCpf () {

		when(service.findCpf("04849633170")).thenReturn(paciente);
		Assert.assertTrue(service.findCpf("04849633170") instanceof Paciente);
	}
	
	@Test
	public void buscarPacientePorCelular() {
		List<Paciente> list = service.findCelular("62994864272");		
		Assert.assertTrue(list instanceof List);
	}

}
