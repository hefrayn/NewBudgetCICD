package newbudget.NewBudgetCICD.ControllerFE;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import newbudget.NewBudgetCICD.Model.Paciente;
import newbudget.NewBudgetCICD.Services.PacienteService;


@RunWith(MockitoJUnitRunner.class)
public class FEControllerPacienteTest {
	
	@InjectMocks
	private FEControllerPaciente fecontrolerpaciente;
	
	@Mock
	private PacienteService service;
	
	@Mock
	private Paciente paciente;
	
	@Mock
	BindingResult result;
	
	@Test 
	public void ListarPaciente () {
		
		ModelAndView mv = fecontrolerpaciente.findAll("");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void FormulariodeCadastro () {
		
		ModelAndView mv = fecontrolerpaciente.add(paciente,"");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test
	public void salvarPacienteDadosFormularioOK() {

		ModelAndView mv = fecontrolerpaciente.save(paciente, result);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Paciente Salvo Com Sucesso");
	}
	
	@Test
	public void salvarPacienteDadosFaltandoPreencher() {
		when(result.hasErrors()).thenReturn(true);
		ModelAndView mv = fecontrolerpaciente.save(paciente, result);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Verifique os dados do Paciente");
	}
	
	@Test
	public void salvarPacienteDadosOk() {
		when(result.hasErrors()).thenReturn(false);
		ModelAndView mv = fecontrolerpaciente.save(paciente, result);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Paciente Salvo Com Sucesso");
	}

	@Test
	public void deletarPaciente() {
		
		ModelAndView mv = fecontrolerpaciente.delete(5);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Paciente deletado Com Sucesso.");
		
	}
	
	@Test
	public void editarPaciente() {
		ModelAndView mv = fecontrolerpaciente.edit(5);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Edite os dados do Paciente");
	}
	
	@Test 
	public void BuscarPacientePorID () {
		
		ModelAndView mv = fecontrolerpaciente.buscarPacientePorCodigo(5);
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarPacientePorNome () {
		
		ModelAndView mv = fecontrolerpaciente.buscarPacientePorNome("Hefrayn Antero");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarPacientePorDatanascimento () {
		
		ModelAndView mv = fecontrolerpaciente.buscarPacientePorDatanascimento("19/04/1993");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarPacientePorCpf () {
		
		ModelAndView mv = fecontrolerpaciente.buscarPacientePorCpf("04849633170");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarPacientePorCelular () {
		
		ModelAndView mv = fecontrolerpaciente.buscarPacientePorCelular("62994864272");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}

}
