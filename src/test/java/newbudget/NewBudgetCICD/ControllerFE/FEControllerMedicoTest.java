package newbudget.NewBudgetCICD.ControllerFE;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import newbudget.NewBudgetCICD.Model.Medico;
import newbudget.NewBudgetCICD.Services.MedicoService;

@RunWith(MockitoJUnitRunner.class)
public class FEControllerMedicoTest {

	@InjectMocks
	private FEControllerMedico fecontrolermedico;
	
	@Mock
	private MedicoService service;
	
	@Mock
	private Medico medico;
	
	@Mock
	BindingResult result;
	
	@Test 
	public void ListarMedico () {
		
		ModelAndView mv = fecontrolermedico.findAll("");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void FormulariodeCadastro () {
		
		ModelAndView mv = fecontrolermedico.add(medico,"");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test
	public void salvarMedicoDadosFormularioOK() {

		ModelAndView mv = fecontrolermedico.save(medico, result);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Medico Salvo Com Sucesso");
	}
	
	@Test
	public void salvarMedicoDadosFaltandoPreencher() {
		when(result.hasErrors()).thenReturn(true);
		ModelAndView mv = fecontrolermedico.save(medico, result);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Verifique os dados do Medico");
	}
	
	@Test
	public void salvarMedicoDadosOk() {
		when(result.hasErrors()).thenReturn(false);
		ModelAndView mv = fecontrolermedico.save(medico, result);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Medico Salvo Com Sucesso");
	}

	@Test
	public void deletarMedico() {
		
		ModelAndView mv = fecontrolermedico.delete(5);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Medico deletado Com Sucesso.");
		
	}
	
	@Test
	public void editarMedico() {
		ModelAndView mv = fecontrolermedico.edit(5);
		Assert.assertTrue(mv.getModel().get("mensagem")=="Edite os dados do Medico");
	}
	
	@Test 
	public void BuscarMedicoPorID () {
		
		ModelAndView mv = fecontrolermedico.buscarMedicoPorCodigo(5);
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarMedicoPorNome () {
		
		ModelAndView mv = fecontrolermedico.buscarMedicoPorNome("Hefrayn Antero");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarMedicoPorDatanascimento () {
		
		ModelAndView mv = fecontrolermedico.buscarMedicoPorDatanascimento("19/04/1993");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarMedicoPorCpf () {
		
		ModelAndView mv = fecontrolermedico.buscarMedicoPorCpf("04849633170");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarMedicoPorCrm () {
		
		ModelAndView mv = fecontrolermedico.buscarMedicoPorCrm("1928");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
	@Test 
	public void BuscarMedicoPorCelular () {
		
		ModelAndView mv = fecontrolermedico.buscarMedicoPorCelular("62994864272");
		Assert.assertTrue(mv instanceof ModelAndView);
		
	}
	
}
